import Hls from 'hls.js'
import { ref } from 'vue'

export enum BtnType {
  play,
  full
}

export const loading = ref<boolean>(true) //加载中
export const dialog = ref(false) //dialog弹窗
export const play_pause = ref<boolean>() //播放暂停状态
export const volume = ref(0) //音量条的初始值

export class HlsPro extends Hls {
  video: HTMLMediaElement
  errsum = 0
  constructor(videoNode: HTMLMediaElement, src: string) {
    super()
    this.video = videoNode
    this.attachMedia(videoNode) //添加节点
    this.loadSource(src) //添加视频地址
    this.on(Hls.Events.ERROR, this.error)
    volume.value = videoNode.volume
    videoNode.addEventListener('play', () => (play_pause.value = false))
    // console.log(Hls.Events)
  }
  error() {
    console.log('错误！')
    dialog.value = true
    this.errsum++
    if (this.errsum > 3) {
      dialog.value = true
      // loading.value = true
      setTimeout(() => {
        this.errsum = 0
        // loading.value = false
      }, 1000)
    }
  }
  btnPress(btn: BtnType): void {
    const vid = this.video
    switch (btn) {
      case BtnType.play: //播放暂停
        if (play_pause.value) {
          vid.play()
        } else {
          vid.pause()
        }
        //不管播放还是暂停都变换按钮图标
        play_pause.value = !play_pause.value
        break
      case BtnType.full:
        //全屏
        vid.requestFullscreen()
        break
    }
  }
}
