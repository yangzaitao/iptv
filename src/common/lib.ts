import { ThemeDefinition } from 'vuetify/lib/framework.mjs'
import { KeyDowmType, ThemeType } from './interface'

enum ColorS {
  /**红色 */
  red = 'red',
  /**粉色 */
  pink = 'pink',
  /**紫色 */
  purple = 'purple',
  /**深紫色 */
  deepPurple = 'deep-purple',
  /**青色 */
  indigo = 'indigo',
  /**蓝色 */
  bule = 'blue',
  /**浅蓝色 */
  lightBule = 'light-blue',
  /**蓝绿色*/
  cyan = 'cyan',
  /**鸭脖绿 */
  teal = 'teal',
  /**绿色 */
  green = 'green',
  /**浅绿色 */
  lightGreen = 'light-green',
  /**亮绿色 */
  lime = 'lime',
  /**黄色 */
  yellow = 'yellow',
  /**琥珀色 */
  amber = 'amber',
  /**橙色 */
  orange = 'orange',
  /**暗橙色 */
  deepOrange = 'deep-orange',
  /**褐色 */
  brown = 'brown',
  /**蓝灰色 */
  blueGrey = 'blue-grey',
  /**灰色 */
  grey = 'grey'
}

class Lighten {
  val: string
  constructor(val: 1 | 2 | 3 | 4 | 5) {
    this.val = Darken.name.toLocaleLowerCase().concat('-', val.toString())
  }
}
class Darken {
  val: string
  constructor(val: 1 | 2 | 3 | 4) {
    this.val = Darken.name.toLocaleLowerCase().concat('-', val.toString())
  }
}
class Accent {
  val: string
  constructor(val: 1 | 2) {
    this.val = Accent.name.toLocaleLowerCase().concat('-', val.toString())
  }
}

class ColorV {
  val: string
  constructor(color: ColorS, type: Accent | Lighten | Darken) {
    this.val = color.concat('-', type.val)
  }
}

/**tab栏的按钮与后台的交互方法 */
export function TabDown(type: KeyDowmType): void {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-ignore
  window.api[type]()
}

/**自定以主题配置 */
const Themes: { [key: string]: ThemeDefinition } = {
  /**白色主题 */
  CustomLight: {
    dark: false,
    colors: {
      background: '#FFFFFF',
      surface: '#FFFFFF',
      primary: '#6200EE',
      'primary-darken-1': '#3700B3',
      secondary: '#03DAC6',
      'secondary-darken-1': '#018786',
      error: '#B00020',
      info: '#2196F3',
      success: '#4CAF50',
      warning: '#FB8C00'
    }
  },
  /**黑色主题 */
  CustomDark: {
    dark: true
    // colors: {
    //   background: '#FDD835',
    //   surface: '#FFEE58',
    //   primary: '#6200EE',
    //   'primary-darken-1': '#3700B3',
    //   secondary: '#03DAC6',
    //   'secondary-darken-1': '#018786',
    //   error: '#B00020',
    //   info: '#2196F3',
    //   success: '#4CAF50',
    //   warning: '#FB8C00'
    // }
  }
}

/**返回一个主题色配置 */
export function getTheme(type: ThemeType): any {
  return {
    defaultTheme: type,
    themes: { ...Themes }
  }
}

new ColorV(ColorS.grey, new Lighten(1))

export const Color = {
  theme: new ColorV(ColorS.blueGrey, new Accent(1)).val,
  theme2: 'deep-purple-accent-4',
  error: 'red',
  txtcolor: 'white',
  videoBgColor: '#1e1e1e'
}
