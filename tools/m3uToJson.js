// 用来m3u转json的脚本
const fs = require('fs')
const readline = require('node:readline')
const path = require('path')

//绝对途径
const input_path = path.join(__dirname, 'm3u')
const out_path = path.join(__dirname, 'out')
fs.readdir(input_path, (err, files) => {
  if (!err) {
    //循环输出file
    files.forEach((file) => {
      //如果是m3u格式就交给Tojson处理
      if (file.match(/.m3u/g) !== null) {
        new Tojson(
          path.join(/*转成相对途径*/ path.basename(input_path), file),
          path.join(path.basename(out_path), `${Math.random().toString(10).slice(2, 6)}.json`)
        )
      }
    })
  }
})

class Tojson {
  constructor(m3uFile, outdir, roSkip = 1) {
    const rl = readline.createInterface({
      input: fs.createReadStream(m3uFile),
      crlfDelay: Infinity
    })
    //总对象群数组
    const arr = []
    //用计数
    let i = 0
    //跳的行数
    let row = roSkip
    //单个节目对象
    let obj = { name: '无name', path: '无url' }
    rl.on('line', (line) => {
      //用来跳行
      if (row > 0) {
        row -= 1
        i = 0
        return
      }
      //清空
      if (i > 1) {
        arr.push(obj)
        i = 0
        obj = { name: '无name', path: '无url' }
      }
      switch (i) {
        case 0:
          obj.name = this.getNmae(line)
          break
        case 1: //path途径
          obj.path = line
          break
      }
      i++
    })
    rl.on('close', () => {
      console.log('执行完毕')
      fs.writeFile(outdir, JSON.stringify(arr), 'utf8', (err) => {
        err && console.log(err)
      })
    })
  }
  getNmae(lineName = '') {
    let name = '无name'
    const regA = new RegExp(/,[\u4e00-\u9fa5]+/g)
    const regB = new RegExp(/,\w+[\u4e00-\u9fa5]+/g)
    const regC = new RegExp(/,\w+/g)
    switch (lineName) {
      case regA.test(lineName) && lineName:
        name = lineName.match(regA)
        break
      case regB.test(lineName) && lineName:
        name = lineName.match(regB)
        break
      case regC.test(lineName) && lineName:
        name = lineName.match(regC)
        break
    }
    return name[0].replace(',', '')
  }
}
