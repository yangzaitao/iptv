import { app, shell, BrowserWindow, ipcMain, Menu } from 'electron'
import { join } from 'path'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import { electronApp, optimizer, is } from '@electron-toolkit/utils'
import icon from '../../resources/icon.png?asset'
import { HttpPro } from '../common/net'
import { KeyDowmType } from '../common/interface'

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true' //屏蔽scf错误警告

const menu = Menu.buildFromTemplate([
  {
    label: app.name,
    submenu: [
      { role: 'about' },
      { type: 'separator' },
      { role: 'hide' },
      { role: 'hideOthers' },
      { role: 'unhide' },
      { type: 'separator' },
      { role: 'quit' }
    ]
  }
])
Menu.setApplicationMenu(menu)

function createWindow(): void {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 960,
    height: 830,
    minWidth: 960,
    minHeight: 830,
    show: false,
    frame: false,
    autoHideMenuBar: true,
    ...(process.platform === 'linux' ? { icon } : {}),
    webPreferences: {
      preload: join(__dirname, '../preload/index.js'),
      sandbox: false
    }
  })

  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
  })

  mainWindow.webContents.setWindowOpenHandler((details) => {
    shell.openExternal(details.url)
    return { action: 'deny' }
  })

  //监听小化全屏关闭三个操作按钮
  ipcMain.on('onTab', (ent, downkey) => {
    ent
    switch (downkey) {
      case KeyDowmType.full:
        mainWindow.setFullScreen(!mainWindow.isFullScreen()) //全屏窗口
        break
      case KeyDowmType.min:
        mainWindow.minimize() //最小化窗口
        break
      case KeyDowmType.close:
        app.exit() //关闭窗口
        break
    }
  })
  //监听网络请求餐宿
  ipcMain.handle('net', async (ent, reqtype, body) => {
    ent
    return new HttpPro(reqtype).send(body)
  })

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
  if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
    mainWindow.loadURL(process.env['ELECTRON_RENDERER_URL'])
  } else {
    mainWindow.loadFile(join(__dirname, '../renderer/index.html'))
  }
}

app.whenReady().then(() => {
  electronApp.setAppUserModelId('com.iptv')
  app.on('browser-window-created', (_, window) => {
    optimizer.watchWindowShortcuts(window)
  })

  createWindow()

  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
