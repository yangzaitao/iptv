import { keyType, videoList, Category, PageType } from '@common/interface'
import { reactive, ref, watch } from 'vue'
import { Staging } from '@common/Storage'
import { PageIdx } from './App'
import VideoData from './lib/VideoData'

//目录分页数据
export const page = reactive({ idx: 1, sum: 0 })
//tab索引
export const tab = ref<number>(0)
//分类加载中
export const loading = ref(true)
//列表加载中
export const VideoListloding = ref(true)
//视频类别
export const listmap = ref<Category[]>([{ _id: '', category: 0, title: '' }])
//视频列表
export const videolist = ref<videoList[]>([
  { _id: '', category: 0, color: '', img: '', like: 0, subTitle: '', title: '', url: '' }
])
watch(
  () => page.idx,
  () => {
    getVideoList(tab.value)
  }
)
/**请求视频分类数据 */
function getCategory(): void {
  new VideoData().category((res) => {
    // console.log(res)
    const CategoryData: Category[] = res.data
    // console.log(CategoryData)
    //设置数据
    listmap.value = CategoryData
    getVideoList(tab.value)
    //隐藏进度条
    setTimeout(() => {
      loading.value = false
    }, 500)
  })
}
/**请求视频列表数据 */
function getVideoList(category: number): void {
  // console.log('当前选择分类为：', category, page.idx)
  videolist.value = [] //清空原来的条目数据
  VideoListloding.value = true //显示进度条
  new VideoData().vidoeList(category, (res) => {
    // console.log(res)
    //设置分页长度
    page.sum = res.length
    // console.log('视频列表数据')
    // console.log(res[page.idx - 1])
    videolist.value = res[page.idx - 1]
    setTimeout(() => {
      VideoListloding.value = false //隐藏进度条
    }, 1000)
    //更新是否在喜欢里面有
    const liveData = new Staging().like
    //如果视频列表为空就reutrn
    if (res[page.idx - 1] === undefined) return
    videolist.value.forEach((item) => {
      if (liveData !== undefined) {
        liveData.forEach((item_) => {
          if (item.url === item_.url) {
            item['islike'] = true
            // console.log(item)
          }
        })
      }
    })
  })
}
/**更新喜欢按钮颜色 */
const Up_videolist = () => {
  if (new Staging().like !== undefined) {
    const likeArr: videoList[] = new Staging().like.filter((item) => item)
    const videArr = JSON.parse(JSON.stringify(videolist.value))
    // console.log(videArr)
    //重置颜色
    videArr.forEach((vide) => (vide['islike'] = false))
    likeArr.forEach((list) => {
      videArr.forEach((vide: { _id: string }) => {
        //重置一下喜欢属性
        if (list._id === vide._id) {
          // console.log('找到相同的元素为', vide)
          vide['islike'] = true
        }
      })
    })
    videolist.value = videArr
    // console.log(videArr)
    // console.log(videolist.value)
  }
}
/**按钮事件监听 */
function keydown(type: keyType, item: any) {
  //json化一下数据
  const item_ = JSON.parse(JSON.stringify(item))
  switch (type) {
    case keyType.live:
      new Staging().addlike(item_)
      Up_videolist()
      break
    case keyType.play:
      new Staging().additem(item_)
      PageIdx.value = PageType.Video
      break
  }
  // console.log(item_)
}
/**根据输入框url播放视频 */
const iptEdit = {
  iptVal: ref(''),
  Customization: () => {
    const item: videoList = {
      _id: '',
      title: '',
      subTitle: '',
      img: '',
      url: '',
      color: '',
      like: 0,
      category: 0
    }
    // console.log(iptVal.value)
    item.title = iptEdit.iptVal.value
    item.url = iptEdit.iptVal.value
    if (new RegExp(/(https:|http:)\/\/\S+.m3u8/g).test(iptEdit.iptVal.value)) {
      console.log('hello')
      new Staging().additem(item)
      PageIdx.value = PageType.Video //转到播放页面
      iptEdit.iptVal.value = ''
    } else {
      alert('地址格式错误!')
    }
  }
}
/**函数合集 */
export const fun = {
  getVideoList,
  getCategory,
  keydown,
  Up_videolist,
  iptEdit
}
