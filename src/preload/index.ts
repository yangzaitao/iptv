import { contextBridge, ipcRenderer } from 'electron'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import { electronAPI } from '@electron-toolkit/preload'
import { KeyDowmType, ReqType } from '../common/interface'

// Custom APIs for renderer
const api = {
  full: (): void => ipcRenderer.send('onTab', KeyDowmType.full),
  min: (): void => ipcRenderer.send('onTab', KeyDowmType.min),
  close: (): void => ipcRenderer.send('onTab', KeyDowmType.close),
  net: (type: ReqType, body?: any): any => ipcRenderer.invoke('net', type, body)
}

// Use `contextBridge` APIs to expose Electron APIs to
// renderer only if context isolation is enabled, otherwise
// just add to the DOM global.
if (process.contextIsolated) {
  try {
    contextBridge.exposeInMainWorld('electron', electronAPI)
    contextBridge.exposeInMainWorld('api', api)
  } catch (error) {
    console.error(error)
  }
} else {
  // @ts-ignore (define in dts)
  window.electron = electronAPI
  // @ts-ignore (define in dts)
  window.api = api
}
