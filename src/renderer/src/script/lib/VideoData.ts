import { CooKie, NetData, Staging } from '@common/Storage'
import { ReqType, SucType, videoList } from '@common/interface'

export default class VideoData {
  //网络请求api
  private net(dz: ReqType) {
    return window.api.net(dz)
  }
  //请求分类数据
  category(callack: (res: any) => void) {
    const categoryData = new NetData().category
    //本地的分类数据为空
    if (categoryData === undefined || new CooKie().isOverDueDate()) {
      //网络请求加载数据
      this.net(ReqType.cetagory).then((res: SucType) => {
        if (res.code === 0) {
          console.log('视频数据接口个请求的数据', res.data)
          //存到本地数据
          new NetData().addcategory(res.data)
          callack(new NetData().category)
        } else {
          console.info('分类列表请求失败！', res.code)
        }
      })
    } else {
      // console.log('已经有数据')
      callack(new NetData().category)
    }
  }
  //网络请求视频列表数据
  vidoeList(category: number, callack: (res: any) => void) {
    let videlListData: { affectedDocs: number; data: videoList[] } = new NetData().list
    const setData = () => {
      if (videlListData) {
        // console.log(videlListData)
        if (videlListData.data === undefined) videlListData.data = []
        let arr = videlListData.data.filter((item) => item.category === category)
        if (category === -1) {
          arr = videlListData.data
        }
        const arrList: Array<videoList[]> = []
        const speed = 9
        let idx = 0
        arr.forEach((item, idx_) => {
          //undefault就添加一个空数组
          if (arrList[idx] === undefined) {
            arrList.push([])
          }
          //添加元素
          arrList[idx].push(item)
          //更新idx索引
          if ((idx_ + 1) % speed === 0) {
            idx += 1
          }
        })
        callack(arrList)
      }
    }
    if (videlListData === undefined || new CooKie().isOverDueDate()) {
      this.net(ReqType.videoAllList).then((res: SucType) => {
        if (res.code === 0) {
          new NetData().addlist(res.data)
          videlListData = res.data
          setData()
        } else {
          console.info('视频列表数据加载错误！')
        }
      })
    } else {
      videlListData = new NetData().list
      if (category === -1) {
        videlListData = { data: new Staging().like, affectedDocs: 0 }
      }
      setData()
    }
  }
}
