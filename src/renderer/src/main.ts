import { createApp } from 'vue'
import App from './App.vue'

// Vuetify
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import { getTheme } from '@common/lib'
import { ThemeType } from '@common/interface'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const vuetify = createVuetify({
  theme: getTheme(ThemeType.light),
  icons: { defaultSet: 'mdi' },
  components,
  directives
})

createApp(App).use(vuetify).mount('#app')
