import { SucType, videoList } from './interface'

export enum StorageType {
  session,
  local
}

enum KEYTYPE {
  /**视频列表 */
  videoList = 'list',
  /**视频分类 */
  videoCategory = 'category',
  cookie = 'cookie',
  /**临时存点击播放的视频 */
  item = 'item',
  /**喜欢的item */
  like = 'like'
}

/**原始存储类 */
export class Storage {
  private storage: globalThis.Storage | undefined
  constructor(type: StorageType) {
    switch (type) {
      case StorageType.local:
        this.storage = localStorage
        break
      case StorageType.session:
        this.storage = sessionStorage
        break
    }
  }
  /**存储obj数据 */
  protected saveData(key: string, obj: any) {
    try {
      this.storage?.setItem(key, JSON.stringify(obj))
    } catch (error) {
      console.log('传的数据不能被json化')
    }
  }
  /**拿取存好的数据 */
  protected getData(key: string) {
    const data = this.storage?.getItem(key)
    let rdata
    try {
      if (data) {
        rdata = JSON.parse(data)
      }
    } catch (error) {
      console.log(`查找${key}的数据出错`, data)
    }
    return rdata
  }
  protected delData(key: string) {
    this.storage?.removeItem(key)
  }
  //清空本地数据
  static clearData() {
    localStorage.clear()
  }
}

/**时间过期类 */
export class CooKie extends Storage {
  constructor() {
    super(StorageType.local)
  }
  /**设置过期时间单位是 1 = 1天*/
  setDate(day = 1) {
    const day_1 = 24 * 60 * 60 * 1000
    const overdueDate = Date.now() + day_1 * day
    this.saveData(KEYTYPE.cookie, { date: overdueDate })
  }
  /**设置的时间是否已经过期 */
  isOverDueDate() {
    const overdueDate = this.getData(KEYTYPE.cookie)?.date
    // console.log('设置的过期时间-当前的时间', overdueDate, Date.now())
    // console.log('是否过期', overdueDate < Date.now())
    return overdueDate < Date.now()
  }
}

/**用来存储视频分类，视频列表数据*/
export class NetData extends Storage {
  constructor() {
    super(StorageType.local)
  }
  get list() {
    return this.getData(KEYTYPE.videoList)
  }
  get category() {
    return this.getData(KEYTYPE.videoCategory)
  }
  /**本地储存列表 */
  addlist(obj: SucType) {
    console.log(obj.data[0])
    new Staging().additem(obj.data[0])
    this.saveData(KEYTYPE.videoList, obj)
  }
  /**本地储存分裂 */
  addcategory(obj: any) {
    new CooKie().setDate(7) //设置7天缓存时间
    this.saveData(KEYTYPE.videoCategory, obj)
  }
  /**删除列表 */
  dellist() {
    this.delData(KEYTYPE.videoList)
  }
  /**删除分类 */
  delcategory() {
    this.delData(KEYTYPE.videoCategory)
  }
}

/**存储喜欢的item,播放按钮按下的item数据 */
export class Staging extends Storage {
  constructor() {
    super(StorageType.local)
  }
  get item(): videoList {
    return this.getData(KEYTYPE.item)
  }
  get like(): videoList[] {
    return this.getData(KEYTYPE.like)
  }
  /**添加删除喜欢的视频对象存在本地 */
  addlike(obj: videoList) {
    const likeArr = [obj]
    // console.log('本地存储的列表', this.like)
    // console.log('喜欢的列表', likeArr)
    //如果没有存储过喜欢视频的列表就创建一个喜欢列表数组
    if (this.like === undefined) {
      this.saveData(KEYTYPE.like, likeArr)
    } else {
      const arr: videoList[] = this.like
      const bol = arr.some((item) => item._id === obj._id) //判断arr里面是否有obj
      //如果已经有这个元素了就找到索引然后根据索引删除这个元素
      if (bol) {
        console.log('本地存储已经有了')
        let idx = 0
        arr.forEach((item, idxx) => {
          if (item._id === obj._id) {
            idx = idxx
          }
        })
        //截取去掉这个元素
        arr.splice(idx, 1)
        this.saveData(KEYTYPE.like, arr)
      } else {
        //如果本地储存里面没有就添加到本地存储
        this.saveData(KEYTYPE.like, arr.concat(likeArr))
      }
    }
  }
  /**临时存储要播放的item */
  additem(obj: any) {
    this.saveData(KEYTYPE.item, obj)
  }
}
