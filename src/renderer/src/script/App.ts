import { BtnsType, PageType } from '@common/interface'
import { ref } from 'vue'
import ControlBa from '@renderer/components/ControlBar.vue'
export const ControlBar = ControlBa
import Video from '@renderer/view/Video.vue'
import Home from '@renderer/view/Home.vue'
import User from '@renderer/view/User.vue'
export const pageArr = [Home, Video, User]

export const PageIdx = ref(PageType.Home)

export const btns: BtnsType[] = [
  {
    icon: 'mdi-view-grid-outline',
    btnName: '节目列表',
    color: 'teal'
  },
  {
    icon: 'mdi-television',
    btnName: '电视',
    color: 'brown'
  },
  {
    icon: 'mdi-cog',
    btnName: '设置',
    color: 'indigo'
  }
]
