///---一下模块只能在electron环境里比如main/index.ts里使用---///
import { net } from 'electron'
import { DataType } from './interface'

/**封装electron的net网络请求 */
export class HttpPro {
  private URL = 'https://fc-mp-faf1c5bd-4804-4d15-90f2-021e39fb4618.next.bspapp.com/'
  private url = ''
  constructor(dir: string) {
    this.url = this.URL.concat(dir)
  }
  send(body: any, method = 'GET'): Promise<DataType> {
    // console.log('url___', this.url)
    // console.log('body___', body)
    return new Promise((res) => {
      const data: DataType = Object.create(null)
      //判断有网络连接
      if (net.isOnline()) {
        const xhr = net.request({ method: method, url: this.url })

        //如果body有参数就加到xhr里面去
        if (body !== undefined) {
          xhr.setHeader('Content-type', 'application/json')
          try {
            xhr.write(JSON.stringify(body))
          } catch (error) {
            data.code = 505
            data.msg = '参数格式化json错误'
            res(data)
          }
        }

        //监听回来的数据
        xhr.on('response', (response) => {
          let buf: Buffer
          response.on('data', (chunk) => (buf = chunk))
          response.on('end', () => {
            data.code = 0
            data.msg = '请求成功!'
            data.data = JSON.parse(buf.toString())
            res(data)
          })
        })
        xhr.end()
      } else {
        data.code = 404
        data.msg = '无网络连接！'
        res(data)
      }
    })
  }
}
