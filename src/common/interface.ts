export enum KeyDowmType {
  /**全屏 */
  full = 'full',
  /**最小化 */
  min = 'min',
  /**关闭 */
  close = 'close'
}

///-----main.ts需要的接口类型-------///
/**主题配置key */
export enum ThemeType {
  light = 'CustomLight',
  dark = 'CustomDark'
}
/**网络请求 */
export interface DataType {
  code: number
  data: any
  msg: string
}
///-----main.ts需要的接口类型-------///

///-----Home.ts需要的接口类型-------///
/**按钮类型 */
export enum keyType {
  live,
  play
}
export enum ReqType {
  /**视频分类数据 */
  cetagory = 'cetagory',
  /**视频列表数据 */
  videolist = 'videolist',
  /** */
  videoAllList = 'videolistall'
}

/**视频类型数据 */
export interface Category {
  _id: string
  title: string
  category: number
}
/**视频列表数据 */
export interface videoList {
  _id: string
  title: string
  subTitle: string
  img: string
  url: string
  color: string
  like: number
  category: number
}

export interface SucType {
  code: number
  msg: string
  data: any
}

export interface VodiType<T> {
  affectedDocs: number
  data: T
}
///-----Home.ts需要的接口类型END-------///

///-----App.ts需要的接口类型Start-------///
export interface BtnsType {
  [key: string]: string //途径
  icon: string
  btnName: string
}
/**页面途径配置 */
export enum PageType {
  Home,
  Video,
  User
}
///-----App.ts需要的接口类型END-------///
