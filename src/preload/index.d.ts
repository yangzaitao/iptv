import { ElectronAPI } from '@electron-toolkit/preload'

type fun = () => void
declare global {
  interface Window {
    electron: ElectronAPI
    api: {
      full: fun
      min: fun
      close: fun
      net: (type: ReqType, body?: any) => Promise<any>
    }
  }
}
